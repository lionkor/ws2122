package com.kortlepel.bank;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.kortlepel.bank.exceptions.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

public class PrivateBankTest {
    PrivateBank privateBank;

    @BeforeEach
    public void initialize() throws
            TransactionAlreadyExistsException,
            AccountDoesNotExistException,
            AccountAlreadyExistsException,
            IOException {
        File Accounts = new File("MyPath");

        if (!Accounts.exists()) {
            assert Accounts.mkdirs();
        }

        File kontoPeterP = new File(Accounts, "Peter P.json");
        File kontoHerbertH = new File(Accounts, "Herbert H.json");

        if (!kontoPeterP.exists()) {
            assert kontoPeterP.createNewFile();
        }
        if (!kontoHerbertH.exists()) {
            assert kontoHerbertH.createNewFile();
        }

        FileWriter kontoPeterPWriter = new FileWriter(kontoPeterP);
        kontoPeterPWriter.write("[]");
        kontoPeterPWriter.close();

        FileWriter kontoHerbertHWriter = new FileWriter(kontoHerbertH);
        kontoHerbertHWriter.write("[]");
        kontoHerbertHWriter.close();
        privateBank = new PrivateBank("SuperBank", 0.02, 0.03, "MyPath");
    }

    @AfterEach
    public void cleanup() {
        File accountsFile = new File("MyPath");
        if (accountsFile.exists()) {
            for (File f : Objects.requireNonNull(accountsFile.listFiles())) {
                if (!f.isDirectory()) {
                    assert f.delete();
                }
            }
        }
    }

    @Test
    public void testWriteAccount() {
        assertDoesNotThrow(() -> {
            privateBank.writeAccount("Peter P");
        });
        assertDoesNotThrow(() -> {
            privateBank.writeAccount("Herbert H");
        });
        File fileOne = new File("MyPath/Peter P.json");
        assertTrue(fileOne.exists());
        File fileTwo = new File("MyPath/Herbert H.json");
        assertTrue(fileTwo.exists());
        /*
        assertThrows(IOException.class, () -> {
            privateBank.writeAccount("Herbert H");
        });
        */
        assertThrows(AccountDoesNotExistException.class, () -> {
            privateBank.writeAccount("invalid");
        });
    }

    @Test
    public void testCtor() {
        assertEquals("SuperBank", privateBank.getName());
        assertEquals(0.02, privateBank.getIncomingInterest());
        assertEquals(0.03, privateBank.getOutgoingInterest());
    }

    @Test
    public void testCopyCtor() throws
            TransactionAlreadyExistsException,
            AccountDoesNotExistException,
            IOException {
        PrivateBank privateBankCopy = new PrivateBank(this.privateBank);
        assertEquals(this.privateBank.getName(), privateBankCopy.getName());
        assertEquals(this.privateBank.getIncomingInterest(), privateBankCopy.getIncomingInterest());
        assertEquals(this.privateBank.getOutgoingInterest(), privateBankCopy.getOutgoingInterest());
    }

    @Test
    public void testCreateAccount() {
        assertThrows(AccountAlreadyExistsException.class, () -> {
            privateBank.createAccount("Peter P");
        });
        assertDoesNotThrow(() -> privateBank.createAccount("Erik E"));
        assertEquals(0, privateBank.getAccountBalance("Erik E"));
    }

    @Test
    public void testCreateAccountWithTransactions() {
        Payment payment = new Payment("01.02.2003", 1000, "Gutschein", 0.01, 0.02);
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(payment);
        assertThrows(AccountAlreadyExistsException.class, () -> {
            privateBank.createAccount("Peter P", transactions);
        });
        assertDoesNotThrow(() -> privateBank.createAccount("Erik E", transactions));
        assertEquals(980.0, privateBank.getAccountBalance("Erik E"));
    }

    @Test
    public void testAddTransaction() {
        Payment paymentOne = new Payment("03.04.2005", 10, "Schulden Zurueck", 0.01, 0.02);
        Payment paymentTwo = new Payment("06.07.2008", 225, "Beitrag Miete", 0.01, 0.02);
        assertThrows(AccountDoesNotExistException.class, () -> {
            privateBank.addTransaction("Erik E", paymentTwo);
        });
        assertThrows(TransactionAlreadyExistsException.class, () -> {
            privateBank.addTransaction("Peter P", paymentOne);
            privateBank.addTransaction("Peter P", paymentOne);
        });
        assertDoesNotThrow(() -> privateBank.addTransaction("Herbert H", paymentOne));
        assertTrue(privateBank.containsTransaction("Herbert H", paymentOne));
    }

    @Test
    public void testRemoveTransaction() {
        Payment paymentOne = new Payment("03.04.2005", 10, "Schulden Zurueck", 0.01, 0.02);
        assertThrows(TransactionDoesNotExistException.class, () -> privateBank.removeTransaction("Herbert H", paymentOne));
        assertDoesNotThrow(() -> privateBank.addTransaction("Peter P", paymentOne));
        assertTrue(privateBank.containsTransaction("Peter P", paymentOne));
        assertDoesNotThrow(() -> privateBank.removeTransaction("Peter P", paymentOne));
        assertFalse(privateBank.containsTransaction("Peter P", paymentOne));
    }

    @Test
    public void testContainsTransaction() {
        Payment paymentOne = new Payment("03.04.2005", 10, "Schulden Zurueck", 0.01, 0.02);
        assertFalse(privateBank.containsTransaction("Peter P", paymentOne));
        assertDoesNotThrow(() -> privateBank.addTransaction("Peter P", paymentOne));
        assertTrue(privateBank.containsTransaction("Peter P", paymentOne));
    }

    @Test
    public void testGetAccountBalance() throws TransactionAlreadyExistsException, AccountDoesNotExistException, IOException {
        OutgoingTransfer outgoingTransfer = new OutgoingTransfer("03.04.2005", 10, "Schulden Zurueck", "Peter P", "Herbert H");
        privateBank.addTransaction("Peter P", outgoingTransfer);
        assertEquals(-10, privateBank.getAccountBalance("Peter P"));

        IncomingTransfer iT = new IncomingTransfer("03.04.2005", 10, "Schulden Zurueck", "Herbert H", "Peter P");
        privateBank.addTransaction("Herbert H", iT);
        assertEquals(10, privateBank.getAccountBalance("Herbert H"));
    }

    @Test
    public void testGetTransactions() {
        Payment paymentOne = new Payment("03.04.2005", 10, "Schulden Zurueck", 0.01, 0.02);
        Payment paymentTwo = new Payment("06.07.2008", 225, "Beitrag Miete", 0.01, 0.02);
        OutgoingTransfer outgoingTransfer = new OutgoingTransfer("03.04.2005", 10, "Schulden Zurueck", "Peter P", "Herbert H");

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(paymentOne);
        transactions.add(paymentTwo);
        transactions.add(outgoingTransfer);

        assertDoesNotThrow(() -> {
            privateBank.addTransaction("Peter P", paymentOne);
            privateBank.addTransaction("Peter P", paymentTwo);
            privateBank.addTransaction("Peter P", outgoingTransfer);
        });

        assertEquals(transactions, privateBank.getTransactions("Peter P"));
    }

    @Test
    public void testGetTransactionsSorted() {
        Payment paymentOne = new Payment("03.04.2005", 10, "Schulden Zurueck", 0.01, 0.02);
        Payment paymentTwo = new Payment("06.07.2008", 225, "Beitrag Miete", 0.01, 0.02);
        OutgoingTransfer outgoingTransfer = new OutgoingTransfer("03.04.2005", 10, "Schulden Zurueck", "Peter P", "Herbert H");

        List<Transaction> transactionsSortedAscending = new ArrayList<>();
        transactionsSortedAscending.add(outgoingTransfer);
        transactionsSortedAscending.add(paymentOne);
        transactionsSortedAscending.add(paymentTwo);

        assertDoesNotThrow(() -> {
            privateBank.addTransaction("Peter P", paymentOne);
            privateBank.addTransaction("Peter P", paymentTwo);
            privateBank.addTransaction("Peter P", outgoingTransfer);
        });

        assertEquals(transactionsSortedAscending, privateBank.getTransactionsSorted("Peter P", true));
    }

    @Test
    public void testGetTransactionsByType() {
        Payment paymentOne = new Payment("03.04.2005", 10, "Schulden Zurueck", 0.01, 0.02);
        Payment paymentTwo = new Payment("06.07.2008", 225, "Beitrag Miete", 0.01, 0.02);
        OutgoingTransfer outgoingTransfer = new OutgoingTransfer("03.04.2005", 10, "Schulden Zurueck", "Peter P", "Herbert H");

        List<Transaction> transactionsNegative = new ArrayList<>();
        transactionsNegative.add(outgoingTransfer);

        List<Transaction> transactionsPositive = new ArrayList<>();
        transactionsPositive.add(paymentOne);
        transactionsPositive.add(paymentTwo);

        assertDoesNotThrow(() -> {
            privateBank.addTransaction("Peter P", paymentOne);
            privateBank.addTransaction("Peter P", paymentTwo);
            privateBank.addTransaction("Peter P", outgoingTransfer);
        });

        assertEquals(transactionsNegative, privateBank.getTransactionsByType("Peter P", false));
        assertEquals(transactionsPositive, privateBank.getTransactionsByType("Peter P", true));
    }
}
