package com.kortlepel.bank;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PaymentTest {
    private Payment payment;

    @BeforeEach
    public void initialize() {
        payment = new Payment("01.02.2003", 456, "a", 0.01, 0.02);
    }

    @Test
    public void testKonstruktor() {
        assertEquals("01.02.2003", payment.getDate());
        assertEquals(456, payment.getAmount());
        assertEquals("a", payment.getDescription());
        assertEquals(0.01, payment.getIncomingInterest());
        assertEquals(0.02, payment.getOutgoingInterest());
    }

    @Test
    public void testCopyKonstruktor() {
        Payment PCopy = new Payment(payment);
        assertEquals(payment.getDate(), PCopy.getDate());
        assertEquals(payment.getAmount(), PCopy.getAmount());
        assertEquals(payment.getDescription(), PCopy.getDescription());
        assertEquals(payment.getIncomingInterest(), PCopy.getIncomingInterest());
        assertEquals(payment.getOutgoingInterest(), PCopy.getOutgoingInterest());
    }

    @Test
    public void testCalculate() {
        assertEquals(451.44, payment.calculate());
    }

    @Test
    public void testEquals() {
        Payment notTheSame = new Payment("30.03.3003", 112, "a", 0.2, 0.4);
        assertTrue(payment.equals(payment));
        assertFalse(payment.equals(notTheSame));
    }

    @Test
    public void testToString() {
        String expectedString = "date='01.02.2003' amount=451.44 description='a' incomingInterest=0.01 outgoingInterest=0.02";
        assertEquals(expectedString, payment.toString());
    }
}
