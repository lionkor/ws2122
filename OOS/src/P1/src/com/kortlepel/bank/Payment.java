package com.kortlepel.bank;

// A payment
public class Payment {
    // The date of the payment
    private String date;
    // The amount of the payment
    private double amount;
    // The description of the payment
    private String description;
    // The incoming interest of a deposit
    private double incomingInterest;
    // The outgoing interest of a withdrawal
    private double outgoingInterest;

    // The date of payment
    public String getDate() {
        return date;
    }

    // Set the date of payment
    public void setDate(String date) {
        this.date = date;
    }

    // The amount of the payment
    public double getAmount() {
        return amount;
    }

    // Set the amount of the payment
    public void setAmount(double amount) {
        this.amount = amount;
    }

    // The description of the payment
    public String getDescription() {
        return description;
    }

    // Set the description of the payment
    public void setDescription(String description) {
        this.description = description;
    }

    // The incoming interest of the payment
    public double getIncomingInterest() {
        return incomingInterest;
    }

    // Set the incoming interest of the payment
    public void setIncomingInterest(double incomingInterest) {
        this.incomingInterest = incomingInterest;
    }

    // The outgoing interest of the payment
    public double getOutgoingInterest() {
        return outgoingInterest;
    }

    // Set the outgoing interest of the payment
    public void setOutgoingInterest(double outgoingInterest) {
        this.outgoingInterest = outgoingInterest;
    }

    // Constructor which takes date, amount, description of the payment
    public Payment(String date, double amount, String description) {
        this.date = date;
        this.amount = amount;
        this.description = description;
    }

    // Constructor which takes date, amount, description, incoming interest and outgoing interest of the payment
    public Payment(String date, double amount, String description, double incomingInterest, double outgoingInterest) {
        this(date, amount, description);
        this.incomingInterest = incomingInterest;
        this.outgoingInterest = outgoingInterest;
    }

    // Default copy constructor, copies the payment exactly
    public Payment(Payment other) {
        this(other.date, other.amount, other.description, other.incomingInterest, other.outgoingInterest);
    }

    // Prints the object to the System.out stream
    public void printObject() {
        System.out.println("Payment '" + description + "' of " + amount + ", incoming interest " + incomingInterest + ", outgoing interest "
                + outgoingInterest + " on " + date);
    }
}
