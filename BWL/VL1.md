# Bilanz

Aktiva: "Wo ist das Geld?"
	- Anlagevermögen
		- Sachanlagen
		- Finanzanlagen
	- Umlaufvermögen
		- Vorräte
		- Forderungen
		- Wertpapiere
		- Kassenbestand
		- Guthaben bei Kreditinstituten

Passiva: "Woher ist das Geld?"
	- Eigenkapital
	- Fremdkapital
		- Verbindlichkeit gegenüber Banken
		- Verbindlickeit gegenüber Lieferanten


|Aktiva| = |Passiva|


# Rücklagen 
... sind zweckgebundenes Eigenkapital
... sind Überschüsse aus wirtschftl. Tätigkeit
... sind Teil des Eigenkapitals
Kapitalrücklagen entstehen duch Agios bei der Ausgabe von Anteilen, Aktien etc.
Gewinnrücklagen werden aus nicht ausgeschütteten Gewinnen gebildet


# Rückstellungen
... sind zweckgebundenes Fremdkapital
... sind zum Zeitpunkt der Bilanzaufstelling noch nicht genau bestimmt
Unbekannt ist entweder der Grund, der Auszahlungszeitpunkt oder die Höhe
Nach HGB gibt es Pflichten zur Bildung von Rückstellungen
Rückstellungen:
- für ungewisse Verbindlichkeiten
- für drohende Verluste aus schwebenden Geschäften
- für unterlassene Instandhaltung und Abraumbeseitigung
- für Gewährleistungen ohne rechtliche Verpflichtung

# Rechnungsabgrenzungsposten
Ein Unternehmen zahlt im Dezember schon die erste
Dreimonatsmiete für das kommende Jahr  Im Dezember stehen
hohe Kosten die den Gewinn mindern, obwohl die Kosten erst im
Januar, Februar und März wirtschaftlich verursacht werden


# GuV - Gewinn- und Verlustrechnung

???

# Jahresabschluss



Durch den Jahresabschluss wird der wirtschaftliche Erfolg eines
Unternehmens bemessen
Grundlage für zukünftige Planungen und Entscheidungen
Verpflichtung zur Aufstellung des Jahresabschlusses

 Einzelkaufleute: Bilanz, GuV

 Personengesellschaften (OHG, KG): Bilanz, GuV

 Kapitalgesellschaften (GmbH, AG, KgaA): Bilanz, GuV, Anhang,
(zusätzlich erstellen deines Lageberichts)
Personenhandelsgesellschaften, bei denen nicht mindestens eine
natürliche Person als Vollhafter beteiligt ist (insbes. GmbH & Co.
KG), unterliegen den Rechnungslegungsvorschriften für
Kapitalgesellschaften
Ausschüttungsbemessungsfunktion: Ermittlung des
Ausschüttungsbetrages an die Anteilseigner
Informationsfunktion für Dritte: Anteilseigner, Lieferanten,
Kunden, Arbeitnehmer, Gebietskörperschaften