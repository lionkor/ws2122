package com.kortlepel.bank;

import com.kortlepel.bank.PrivateBank;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        PrivateBank bank = new PrivateBank("MyBank", 0.01, 0.02, "myDirectory");
        bank.createAccount("Peter");
        bank.writeAccount("Peter");
        PrivateBank bank2 = new PrivateBank("MyBank2", 0.01, 0.02, "myDirectory");
        bank2.readAccounts();
    }
}
