package com.kortlepel.bank;

/**
 * Represents a Transaction.
 *
 * @author Lion Kortlepel
 */
public abstract class Transaction implements CalculateBill {
    protected String date;
    protected double amount;
    protected String description;

    /**
     * Gets the date of the Transaction
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Gets the amount of the Transaction
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Gets the description of the Transaction
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the date of the Transaction
     * @param date new date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Sets the amount of the Transaction.
     * @param amount new amount
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * Sets the description of the Transaction
     * @param description new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Constructs a new Transaction
     * @param date date of the Transaction
     * @param amount amount of the Transaction
     * @param description description of the Transaction
     */
    public Transaction(String date, double amount, String description) {
        this.date = date;
        this.amount = amount;
        this.description = description;
    }

    /**
     * Converts the transaction to a human-readable string.
     * @return human-readable string
     */
    @Override
    public String toString() {
        return "date='" + date + '\'' +
                " amount=" + calculate() +
                " description='" + description + '\'';
    }

    /**
     * Whether two Transactions are the same.
     * @param object other Transaction to compare to
     * @return true if same, false if not
     */
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || object.getClass() != getClass()) return false;
        Transaction transaction = (Transaction) object;
        return amount == transaction.amount &&
                date.equals(transaction.date) &&
                description.equals(transaction.description);
    }
}
