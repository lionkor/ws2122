package com.kortlepel;

import com.kortlepel.bank.Payment;
import com.kortlepel.bank.Transfer;

public class Main {

    public static void main(String[] args) {
        String date = "11.01.2081";
        String description = "Awesome Description";
        double amount = 1000;
        double incomingInterest = 0.05;
        double outgoingInterest = 0.1;
        String sender = "John Doe";
        String recipient = "Jane Doe";

        Payment shortConstructedPayment = new Payment(date, amount, description);
        System.out.println(shortConstructedPayment);

        Payment longConstructedPayment = new Payment(date, amount, description, incomingInterest, outgoingInterest);
        System.out.println(longConstructedPayment);

        Payment copiedPayment = new Payment(longConstructedPayment);
        System.out.println(copiedPayment);

        Payment longConstructedPaymentNegative = new Payment(date, -amount, description, incomingInterest, outgoingInterest);
        System.out.println(longConstructedPaymentNegative);

        Transfer shortConstructedTransfer = new Transfer(date, amount, description);
        System.out.println(shortConstructedTransfer);

        Transfer longConstructedTransfer = new Transfer(date, amount, description, sender, recipient);
        System.out.println(longConstructedTransfer);

        Transfer copiedTransfer = new Transfer(longConstructedTransfer);
        System.out.println(copiedTransfer);
    }
}
