package com.kortlepel;

import com.kortlepel.bank.Payment;
import com.kortlepel.bank.Transfer;

public class Main {

    public static void main(String[] args) {
        String date = "11.01.2081";
        String description = "Awesome Description";
        double amount = 64.128;
        double incomingInterest = 0.5;
        double outgoingInterest = 0.10;
        String sender = "John Doe";
        String recipient = "Jane Doe";

        Payment shortConstructedPayment = new Payment(date, amount, description);
        shortConstructedPayment.printObject();

        Payment longConstructedPayment = new Payment(date, amount, description, incomingInterest, outgoingInterest);
        longConstructedPayment.printObject();

        Payment copiedPayment = new Payment(longConstructedPayment);
        copiedPayment.printObject();

        Transfer shortConstructedTransfer = new Transfer(date, amount, description);
        shortConstructedTransfer.printObject();

        Transfer longConstructedTransfer = new Transfer(date, amount, description, sender, recipient);
        longConstructedTransfer.printObject();

        Transfer copiedTransfer = new Transfer(longConstructedTransfer);
        copiedTransfer.printObject();
    }
}
