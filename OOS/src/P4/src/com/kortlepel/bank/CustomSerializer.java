package com.kortlepel.bank;

import com.google.gson.*;

import java.lang.reflect.Type;

public class CustomSerializer implements JsonSerializer<Transaction>, JsonDeserializer<Transaction> {
    public final String ClassnameField = "CLASSNAME";
    public final String InstanceField = "INSTANCE";

    @Override
    public JsonElement serialize(Transaction transaction, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject obj = new JsonObject();
        obj.addProperty(ClassnameField, transaction.getClass().getSimpleName());
        // the entire JsonSerializer API is outdated, not supposed to be used like this,
        // and I don't really care. Workaround is giving up and making a new one.
        // Read documentation before you make a task.
        Gson workaroundGson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        obj.add(InstanceField, workaroundGson.toJsonTree(transaction));
        return obj;
    }

    @Override
    public Transaction deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject obj = jsonElement.getAsJsonObject();
        String simpleClassname = obj.get(ClassnameField).getAsString();
        if (Payment.class.getSimpleName().equals(simpleClassname)) {
            return new Gson().fromJson(obj.getAsJsonObject(InstanceField), Payment.class);
        } else if (IncomingTransfer.class.getSimpleName().equals(simpleClassname)) {
            return new Gson().fromJson(obj.getAsJsonObject(InstanceField), IncomingTransfer.class);
        } else if (OutgoingTransfer.class.getSimpleName().equals(simpleClassname)) {
            return new Gson().fromJson(obj.getAsJsonObject(InstanceField), OutgoingTransfer.class);
        } else {
            throw new RuntimeException("Unreachable");
        }
    }
}

