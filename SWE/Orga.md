- siepmann@fh-aachen.de
- VL MiWo 08:15-11:00

# Praktikum

1. Anforderungen klären
2. Lastenheft schreiben, Systemdesign erarbeiten
3. Lastenheft genehmigen lassen
4. Programmieren
5. Testen
6. Vom Kunden abnehmen lassen
7. Project präsentieren

- Projektmanager - Orga, Planung, Soll-Ist, Vorbereitung der Meetings
- Vertriebsmanager - Kontakt zum Kunden
- Qualitätsmanager - QA, Tests
- Technologiemanager - Technologische Fragen, Systemarchitektur

## Projektmanager

2 Leute

Berichtet:

- Aufgaben letzte + kommende Woche
- Komplikationen
- Projektplan, Gantt Chart
- Stundenzahlen
- Schätzung aktueller Restaufwand
- Burn Down Chart

