package com.kortlepel.bank;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.kortlepel.bank.exceptions.AccountAlreadyExistsException;
import com.kortlepel.bank.exceptions.AccountDoesNotExistException;
import com.kortlepel.bank.exceptions.TransactionAlreadyExistsException;
import com.kortlepel.bank.exceptions.TransactionDoesNotExistException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Represents a bank, which holds accounts and their transactions.
 */
public class PrivateBank implements Bank {

    /**
     * Creates a private bank.
     *
     * @param name             name of this bank
     * @param incomingInterest incoming interest for all payments
     * @param outgoingInterest outgoing interest for all payments
     * @param directoryName    name of persistence directory
     */
    public PrivateBank(String name, double incomingInterest, double outgoingInterest, String directoryName) throws IOException {
        this.name = name;
        this.incomingInterest = incomingInterest;
        this.outgoingInterest = outgoingInterest;
        this.directoryName = directoryName;
        this.gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(Transaction.class, new CustomSerializer())
                .serializeNulls()
                .setPrettyPrinting()
                .create();
        readAccounts();
    }

    /**
     * Copies from another private bank, without transactions.
     *
     * @param privateBank bank to copy from
     */
    public PrivateBank(PrivateBank privateBank) throws IOException {
        this(privateBank.name, privateBank.incomingInterest, privateBank.outgoingInterest, privateBank.directoryName);
    }

    /**
     * Writes the specified account to a json file with the account name, in the 'directoryName' directory.
     *
     * @param account account to be saved to json
     * @throws IOException if creating the file fails
     */
    public void writeAccount(String account) throws IOException {
        if (!accountsToTransactions.containsKey(account)) {
            throw new AccountDoesNotExistException();
        }
        File path = new File(directoryName);
        if (!path.exists()) {
            if (!path.mkdirs()) {
                throw new IOException("Couldn't create path");
            }
        }
        File file = new File(directoryName + "/" + account + ".json");
        if (!file.exists()) {
            if (!file.createNewFile()) {
                throw new IOException("Couldn't create file");
            }
        }
        FileWriter writer = new FileWriter(file);
        writer.write(gson.toJson(getTransactions(account)));
        writer.close();
    }

    /**
     * Reads all accounts from json files in the 'directoryName' directory.
     * The filename of the json files becomes the account name.
     *
     * @throws IOException
     */
    public void readAccounts() throws IOException {
        File path = new File(directoryName);
        if (!path.exists()) {
            if (!path.mkdirs()) {
                throw new IOException("Can't create directory.");
            }
        }
        for (File file : Objects.requireNonNull(path.listFiles())) {
            if (file.isFile()) {
                String accountName = file.getName().substring(0, file.getName().lastIndexOf('.'));
                Type type = new TypeToken<List<Transaction>>() {
                }.getType();
                List<Transaction> transactionList = gson.fromJson(new FileReader(file), type);
                createAccount(accountName, transactionList);
            }
        }
    }

    /**
     * Name of the bank.
     *
     * @return name of the bank
     */
    public String getName() {
        return name;
    }

    /**
     * Incoming interest of payments for this bank.
     *
     * @return incoming interest
     */
    public double getIncomingInterest() {
        return incomingInterest;
    }

    /**
     * Outgoing interest of payments for this bank.
     *
     * @return outgoing interest
     */
    public double getOutgoingInterest() {
        return outgoingInterest;
    }

    /**
     * Sets the name of this bank.
     *
     * @param name new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the incoming interest of this bank.
     *
     * @param incomingInterest new incoming interest
     */
    public void setIncomingInterest(double incomingInterest) {
        this.incomingInterest = incomingInterest;
    }

    /**
     * Sets the outgoing interest of this bank.
     *
     * @param outgoingInterest new outgoing interest
     */
    public void setOutgoingInterest(double outgoingInterest) {
        this.outgoingInterest = outgoingInterest;
    }

    /**
     * Creates the account with the specified name.
     *
     * @param account the account to be added
     * @throws AccountAlreadyExistsException account already exists
     */
    @Override
    public void createAccount(String account) throws AccountAlreadyExistsException, IOException {
        if (accountsToTransactions.containsKey(account)) {
            throw new AccountAlreadyExistsException();
        } else {
            accountsToTransactions.put(account, new ArrayList<>());
            writeAccount(account);
        }
    }

    /**
     * Creates the account with the specified name and a list of transactions.
     *
     * @param account      the account to be added
     * @param transactions transactions to be added to this account.
     * @throws AccountAlreadyExistsException account already exists
     */
    @Override
    public void createAccount(String account, List<Transaction> transactions) throws AccountAlreadyExistsException, IOException {
        if (accountsToTransactions.containsKey(account)) {
            throw new AccountAlreadyExistsException();
        } else {
            for (Transaction transaction : transactions) {
                if (transaction instanceof Payment) {
                    ((Payment) transaction).setIncomingInterest(incomingInterest);
                    ((Payment) transaction).setOutgoingInterest(outgoingInterest);
                }
            }
            accountsToTransactions.put(account, transactions);
            writeAccount(account);
        }
    }

    /**
     * Adds a transaction to an account.
     *
     * @param account     the account to which the transaction is added
     * @param transaction the transaction which is added to the account
     * @throws TransactionAlreadyExistsException transaction already exists
     * @throws AccountDoesNotExistException      account doesn't exist
     */
    @Override
    public void addTransaction(String account, Transaction transaction) throws TransactionAlreadyExistsException, AccountDoesNotExistException, IOException {
        if (!accountsToTransactions.containsKey(account)) {
            throw new AccountDoesNotExistException();
        } else if (containsTransaction(account, transaction)) {
            throw new TransactionAlreadyExistsException();
        } else {
            if (transaction instanceof Payment) {
                ((Payment) transaction).setIncomingInterest(incomingInterest);
                ((Payment) transaction).setOutgoingInterest(outgoingInterest);
            }
            accountsToTransactions.get(account).add(transaction);
            writeAccount(account);
        }
    }

    /**
     * Removes a transaction from the specified account.
     *
     * @param account     the account from which the transaction is removed
     * @param transaction the transaction which is removed from the account
     * @throws TransactionDoesNotExistException transaction doesn't exist
     */
    @Override
    public void removeTransaction(String account, Transaction transaction) throws TransactionDoesNotExistException, IOException {
        if (containsTransaction(account, transaction)) {
            if (transaction instanceof Payment) {
                ((Payment) transaction).setIncomingInterest(incomingInterest);
                ((Payment) transaction).setOutgoingInterest(outgoingInterest);
            }
            accountsToTransactions.get(account).remove(transaction);
            writeAccount(account);
        } else {
            throw new TransactionDoesNotExistException();
        }
    }

    /**
     * Whether the specified account already contains this transaction.
     *
     * @param account     the account from which the transaction is checked
     * @param transaction the transaction to be chcked
     * @return whether the transaction exists in this account
     */
    @Override
    public boolean containsTransaction(String account, Transaction transaction) {
        if (transaction instanceof Payment) {
            ((Payment) transaction).setIncomingInterest(incomingInterest);
            ((Payment) transaction).setOutgoingInterest(outgoingInterest);
        }
        return accountsToTransactions.containsKey(account) && accountsToTransactions.get(account).contains(transaction);
    }

    /**
     * Returns the balance for an account, calculated from all transactions-
     *
     * @param account the selected account
     * @return balance of the account
     */
    @Override
    public double getAccountBalance(String account) {
        return getTransactions(account).stream().mapToDouble(CalculateBill::calculate).sum();
    }

    /**
     * Returns all transactions for this account.
     *
     * @param account the selected account
     * @return all transactions for this account
     */
    @Override
    public List<Transaction> getTransactions(String account) {
        return accountsToTransactions.get(account);
    }

    /**
     * Returns all transactions for this account, sorted.
     *
     * @param account the selected account
     * @param asc     selects if the transaction list is sorted ascending or descending
     * @return all transactions for this account, sorted
     */
    @Override
    public List<Transaction> getTransactionsSorted(String account, boolean asc) {
        List<Transaction> transactions = accountsToTransactions.get(account);
        Comparator<Transaction> comparator = Comparator.comparing(Transaction::calculate);
        if (!asc) {
            comparator = comparator.reversed();
        }
        transactions.sort(comparator);
        return transactions;
    }

    /**
     * Returns all transactions that are either positive or negative.
     *
     * @param account  the selected account
     * @param positive selects if positive  or negative transactions are listed
     * @return all transactions which are either positive or negative
     */
    @Override
    public List<Transaction> getTransactionsByType(String account, boolean positive) {
        return getTransactions(account).stream().filter(transaction -> {
            if (positive) {
                return transaction.calculate() >= 0;
            } else {
                return transaction.calculate() < 0;
            }
        }).toList();
    }

    /**
     * Human-readable string which represents this bank.
     *
     * @return human-readable string
     */
    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", incomingInterest=" + incomingInterest +
                ", outgoingInterest=" + outgoingInterest +
                ", accountsToTransactions=" + accountsToTransactions;
    }

    /**
     * Whether this bank is equal to another.
     *
     * @param o bank to be compared to
     * @return whether they are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrivateBank objectAsBank = (PrivateBank) o;
        if (objectAsBank.incomingInterest != incomingInterest
                || objectAsBank.outgoingInterest != outgoingInterest) {
            return false;
        }
        if (!Objects.equals(name, objectAsBank.name)) {
            return false;
        }
        return accountsToTransactions.equals(objectAsBank.accountsToTransactions);
    }

    /**
     * Name of the directory where accounts will be persisted.
     */
    private String directoryName;
    /**
     * Name of this bank.
     */
    private String name;
    /**
     * Incoming interest for transfers in this bank.
     */
    private double incomingInterest;
    /**
     * Outgoing interest for transfers in this bank.
     */
    private double outgoingInterest;
    /**
     * Transactions for each account.
     */
    private Map<String, List<Transaction>> accountsToTransactions = new HashMap<>();
    /**
     * Internal GSON instance used for serialization. Implementation detail, no access.
     */
    Gson gson;
}
