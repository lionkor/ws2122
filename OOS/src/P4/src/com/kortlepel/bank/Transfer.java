package com.kortlepel.bank;

/**
 * Represents a Transfer.
 * @author Lion Kortlepel
 */
public class Transfer extends Transaction {
    private String sender;
    private String recipient;

    /**
     * Gets the recipient of the Transfer
     * @return recipient of the Transfer
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     * Gets the sender of the Transfer
     * @return sendero of the Transfer
     */
    public String getSender() {
        return sender;
    }

    /**
     * Sets the recipient of the Transfer
     * @param recipient new recipient
     */
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    /**
     * Sets the sender of the Transfer
     * @param sender new sender
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * Constructs a new Transfer
     * @param date date of the Transfer
     * @param amount amount of the Transfer
     * @param description description of the Transfer
     */
    public Transfer(String date, double amount, String description) {
        super(date, amount, description);
    }

    /**
     * Constructs a new Transfer with sender and recipient
     * @param date date of the Transfer
     * @param amount amount of the Transfer
     * @param description description of the Transfer
     * @param sender sender of the Transfer
     * @param recipient recipient of the Transfer
     */
    public Transfer(String date, double amount, String description, String sender, String recipient) {
        super(date, amount, description);
        this.sender = sender;
        this.recipient = recipient;
    }

    /**
     * Copies the Transfer
     * @param other Transfer to copy
     */
    public Transfer(Transfer other) {
        this(other.date, other.amount, other.description, other.sender, other.recipient);
    }

    /**
     * Just the amount
     * @return amount
     */
    @Override
    public double calculate() {
        return amount;
    }

    /**
     * Creates a human-readable string describing this Transfer
     * @return human-readable string describing this Transfer
     */
    @Override
    public String toString() {
        return super.toString() +
                " sender='" + sender + "'" +
                " recipient='" + recipient + "'";
    }

    /**
     * Whether two Transfers are equal
     * @param o Transfer to compare to
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Transfer transfer = (Transfer) o;

        return sender.equals(transfer.sender) && recipient.equals(transfer.recipient);
    }
}