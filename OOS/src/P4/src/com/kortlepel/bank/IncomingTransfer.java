package com.kortlepel.bank;

public class IncomingTransfer extends Transfer {

    /**
     * Constructs a new Transfer
     *
     * @param date        date of the Transfer
     * @param amount      amount of the Transfer
     * @param description description of the Transfer
     */
    public IncomingTransfer(String date, double amount, String description) {
        super(date, amount, description);
    }

    /**
     * Constructs a new Transfer with sender and recipient
     *
     * @param date        date of the Transfer
     * @param amount      amount of the Transfer
     * @param description description of the Transfer
     * @param sender      sender of the Transfer
     * @param recipient   recipient of the Transfer
     */
    public IncomingTransfer(String date, double amount, String description, String sender, String recipient) {
        super(date, amount, description, sender, recipient);
    }

    /**
     * Copies the Transfer
     *
     * @param other Transfer to copy
     */
    public IncomingTransfer(Transfer other) {
        super(other);
    }

    @Override
    public double calculate() {
        return +super.calculate();
    }
}
