package com.kortlepel.bank;

/**
 * Interface for anything that can be calculated.
 * @author Lion Kortlepel
 */
public interface CalculateBill {
    /**
     * Calculates the bill.
     * @return the calculated value
     */
    public double calculate();
}
