package com.kortlepel.bank;

/**
 * Represents a Payment.
 * @author Lion Kortlepel
 */
public class Payment extends Transaction {
    private double incomingInterest;
    private double outgoingInterest;

    /**
     * Gets the incoming interest of the Payment.
     * @return incoming interest
     */
    public double getIncomingInterest() {
        return incomingInterest;
    }

    /**
     * Sets the incoming interest of the Payment.
     * @param incomingInterest new incoming interest
     */
    public void setIncomingInterest(double incomingInterest) {
        this.incomingInterest = incomingInterest;
    }

    /**
     * Gets outgoing interest of the Payment.
     * @return outgoing interest
     */
    public double getOutgoingInterest() {
        return outgoingInterest;
    }

    /**
     * Sets the outgoing interest of the Payment.
     * @param outgoingInterest new outgoing interest
     */
    public void setOutgoingInterest(double outgoingInterest) {
        this.outgoingInterest = outgoingInterest;
    }

    /**
     * Constructs a new Payment
     * @param date date of the payment
     * @param amount amount of the payment
     * @param description description of the payment
     */
    public Payment(String date, double amount, String description) {
        super(date, amount, description);
    }

    /**
     * Constructs a new Payment with interest
     * @param date date of the Payment
     * @param amount amount of the Payment
     * @param description description of the Payment
     * @param incomingInterest incoming interest of the Payment
     * @param outgoingInterest outgoing interest of the Payment
     */
    public Payment(String date, double amount, String description, double incomingInterest, double outgoingInterest) {
        super(date, amount, description);
        this.incomingInterest = incomingInterest;
        this.outgoingInterest = outgoingInterest;
    }

    /**
     * Copies the Payment
     * @param other Payment to copy
     */
    public Payment(Payment other) {
        this(other.date, other.amount, other.description, other.incomingInterest, other.outgoingInterest);
    }

    /**
     * Calculates the real amount based on interest.
     * @return real amount based on interest
     */
    @Override
    public double calculate() {
        double result = amount;
        if (amount > 0) {
            // Einzahlung
            result -= amount * incomingInterest;
        } else {
            // Auszahlung
            result += amount * outgoingInterest;
        }
        return result;
    }

    /**
     * Creates a human-readable string describing this Payment.
     * @return human-readable string describing this Payment
     */
    @Override
    public String toString() {
        return super.toString() +
                " incomingInterest=" + incomingInterest +
                " outgoingInterest=" + outgoingInterest;
    }

    /**
     * Compares two Payments
     * @param object other Payment to compare to
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;

        Payment payment = (Payment) object;

        return payment.incomingInterest == incomingInterest
                && payment.outgoingInterest == outgoingInterest;
    }
}
