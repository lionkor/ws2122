package com.kortlepel.bank;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TransferTest {
    Transfer transfer;

    @BeforeEach
    public void initialize() {
        transfer = new Transfer("01.02.2003", 300, "Transfer", "Peter P", "Herbert H");
    }

    @Test
    public void testCtor() {
        assertEquals("01.02.2003", transfer.getDate());
        assertEquals(300, transfer.getAmount());
        assertEquals("Transfer", transfer.getDescription());
        assertEquals("Peter P", transfer.getSender());
        assertEquals("Herbert H", transfer.getRecipient());
    }

    @Test
    public void testCopyCtor() {
        Transfer TCopy = new Transfer(transfer);
        assertEquals(transfer.getDate(), TCopy.getDate());
        assertEquals(transfer.getAmount(), TCopy.getAmount());
        assertEquals(transfer.getDescription(), TCopy.getDescription());
        assertEquals(transfer.getSender(), TCopy.getSender());
        assertEquals(transfer.getRecipient(), TCopy.getRecipient());
    }

    @Test
    public void testEquals() {
        Transfer U = new Transfer("01.01.2000", 200, "b", "Herbert H", "Peter P");
        assertTrue(transfer.equals(transfer));
        assertFalse(transfer.equals(U));
    }

    @Test
    public void testCalculateIncoming() {
        IncomingTransfer incomingTransfer = new IncomingTransfer("01.02.2003", 300, "Transfer", "Peter P", "Herbert H");
        assertEquals(300, incomingTransfer.calculate());
    }

    @Test
    public void testCalculateOutgoing() {
        OutgoingTransfer outgoingTransfer = new OutgoingTransfer("01.02.2003", 300, "Transfer", "Peter P", "Herbert H");
        assertEquals(-300, outgoingTransfer.calculate());
    }

    @Test
    public void testToString() {
        String expectedString = "date='01.02.2003' amount=300.0 description='Transfer' sender='Peter P' recipient='Herbert H'";
        assertEquals(expectedString, transfer.toString());
    }
}
