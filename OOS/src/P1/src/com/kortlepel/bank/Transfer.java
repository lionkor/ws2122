package com.kortlepel.bank;

// A transfer of an amount from a sender to a recipient
public class Transfer {
    // The date of the transfer
    private String date;
    // The amount of the transfer
    private double amount;
    // The description of the transfer
    private String description;
    // The sender of the transfer
    private String sender;
    // The recipient of the transfer
    private String recipient;

    // The date of the transfer
    public String getDate() {
        return date;
    }

    // The amount of the transfer
    public double getAmount() {
        return amount;
    }

    // The description of the transfer
    public String getDescription() {
        return description;
    }

    // The recipient of the transfer
    public String getRecipient() {
        return recipient;
    }

    // The sender of the transfer
    public String getSender() {
        return sender;
    }

    // Set the date of the transfer
    public void setDate(String date) {
        this.date = date;
    }

    // Set the amount of the transfer
    public void setAmount(double amount) {
        this.amount = amount;
    }

    // Set the description of the transfer
    public void setDescription(String description) {
        this.description = description;
    }

    // Set the recipient of the transfer
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    // Set the sender of the transfer
    public void setSender(String sender) {
        this.sender = sender;
    }

    // Constructor which takes date, amount, description of the transfer
    public Transfer(String date, double amount, String description) {
        this.date = date;
        this.amount = amount;
        this.description = description;
    }

    // Constructor which takes date, amount, description, sender and recipient of the transfer
    public Transfer(String date, double amount, String description, String sender, String recipient) {
        this(date, amount, description);
        this.sender = sender;
        this.recipient = recipient;
    }

    // Default copy constructor, copies the transfer exactly
    public Transfer(Transfer other) {
        this(other.date, other.amount, other.description, other.sender, other.recipient);
    }

    // Prints the object to the System.out stream
    public void printObject() {
        System.out.println("Transfer '" + description + "' of " + amount + " from " + sender + " to "
                + recipient + " on " + date);
    }
}