Insgesamt wird das Lastenheft in Google Docs von allen gleichzeitig bearbeitet, wobei alle die nicht Qualitätsmanager sind nur Vorschläge machen. 

- google docs
- von allen gleichzeitig bearbeitet
- außer qualitätsmanager nur vorschläge
- jeder sieht die änderungen von allen anderen live

- qualitätssicherung dadurch, dass alles durch den qualitätsmanager geht
- qualimanager kann akzeptieren, kommentieren oder zurückweisen

# Wie wird die Qualitätssicherung bewerkstelligt?

Die Qualitätssicherung wird also bewerkstelligt, indem ich als Qualitätsmanager sehen kann, wer was wo geändert hat, und es demnach akzeptieren, kommentieren oder auch zurückweisen kann. Dadurch entsteht ein Workflow, bei dem jeder aktiv beitragen, also schreiben, kann, aber alles am Ende durch eine Qualitätsmanager gehen muss um im Dokument richtig zu erscheinen.

Dadurch ist es auch möglich, in den Vorschlägen von Anderen Fehler zu sehen und direkt Änderungen vorzuschlagen, ohne dass ein Chaos entsteht.

# Wie wird die Kommunikation bewerkstelligt?

Die Kommunikation generell passiert über discord, mit verschiedenen threads für verschiedene Themen, und eben Anrufen.

# Welche Standards sollen gelten?


# Fragen

Wie entscheiden Sie, wann das Lastenheft fertig/vollständig ist?
Wer entscheidet?
