package com.kortlepel;

import com.kortlepel.bank.*;

public class Main {

    public static void check(boolean condition) throws RuntimeException {
        if (!condition) {
            throw new RuntimeException("test failed");
        }
    }

    public static void test_bank(Bank bank) {

    }

    public static void main(String[] args) {
        Bank bank = new PrivateBank("Test Bank", 0.02, 0.03);
        bank.createAccount("A");
        check(bank.getAccountBalance("A") == 0);
        check(bank.getTransactions("A").isEmpty());

        bank.createAccount("B");
        Transfer transferAtoB = new OutgoingTransfer("1.1.2021", 100, "Test A to B", "A", "B");
        bank.addTransaction("A", transferAtoB);
        check(bank.getAccountBalance("A") == -transferAtoB.getAmount());
        check(bank.getTransactions("A").get(0).equals(transferAtoB));
        bank.removeTransaction("A", transferAtoB);
        check(!bank.containsTransaction("A", transferAtoB));
        bank.addTransaction("A", transferAtoB);
        bank.createAccount("B");
        transferAtoB = new IncomingTransfer("1.1.2021", 100, "Test A to B", "A", "B");
        bank.addTransaction("B", transferAtoB);
        check(bank.getAccountBalance("B") == transferAtoB.getAmount());
        check(bank.getTransactions("B").get(0).equals(transferAtoB));

        Bank other = new PrivateBank("Test Bank", 0.02, 0.03);
        other.createAccount("A");
        for (Transaction transaction : bank.getTransactions("A")) {
            other.addTransaction("A", transaction);
        }
        other.createAccount("B");
        for (Transaction transaction : bank.getTransactions("B")) {
            other.addTransaction("B", transaction);
        }
        check(other.equals(bank));
        other = new PrivateBank("Other Bank", 0.03, 0.02);
        check(!other.equals(bank));

        bank.addTransaction("A", new Transfer("2.2.2021", 500, "Test 2", "A", "B"));
        System.out.println("unsorted transactions:       " + bank.getTransactions("A"));
        System.out.println("sorted transactions asc:     " + bank.getTransactionsSorted("A", true));
        System.out.println("sorted transactions desc:    " + bank.getTransactionsSorted("A", false));
        System.out.println("positive transactions:       " + bank.getTransactionsByType("A", true));
        System.out.println("negative transactions:       " + bank.getTransactionsByType("A", false));

        // -----------------------------------

        bank = new PrivateBankAlt("Test Bank", 0.02, 0.03);
        bank.createAccount("A");
        check(bank.getAccountBalance("A") == 0);
        check(bank.getTransactions("A").isEmpty());

        bank.createAccount("B");
        transferAtoB = new Transfer("1.1.2021", 100, "Test A to B", "A", "B");
        bank.addTransaction("A", transferAtoB);
        check(bank.getAccountBalance("A") == -transferAtoB.getAmount());
        check(bank.getTransactions("A").get(0).equals(transferAtoB));
        bank.removeTransaction("A", transferAtoB);
        check(!bank.containsTransaction("A", transferAtoB));
        bank.addTransaction("A", transferAtoB);
        bank.createAccount("B");
        transferAtoB = new Transfer("1.1.2021", 100, "Test A to B", "A", "B");
        bank.addTransaction("B", transferAtoB);
        check(bank.getAccountBalance("B") == transferAtoB.getAmount());
        check(bank.getTransactions("B").get(0).equals(transferAtoB));

        other = new PrivateBankAlt("Test Bank", 0.02, 0.03);
        other.createAccount("A");
        for (Transaction transaction : bank.getTransactions("A")) {
            other.addTransaction("A", transaction);
        }
        other.createAccount("B");
        for (Transaction transaction : bank.getTransactions("B")) {
            other.addTransaction("B", transaction);
        }
        check(other.equals(bank));
    }
}

